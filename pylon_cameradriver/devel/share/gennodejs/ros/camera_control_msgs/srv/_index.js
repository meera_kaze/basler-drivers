
"use strict";

let GetCamProperties = require('./GetCamProperties.js')
let SetBrightness = require('./SetBrightness.js')
let SetFloatValue = require('./SetFloatValue.js')
let SetStringValue = require('./SetStringValue.js')
let SetSleeping = require('./SetSleeping.js')
let SetExposure = require('./SetExposure.js')
let SetIntegerValue = require('./SetIntegerValue.js')
let SetGamma = require('./SetGamma.js')
let SetROI = require('./SetROI.js')
let SetGain = require('./SetGain.js')
let SetBinning = require('./SetBinning.js')

module.exports = {
  GetCamProperties: GetCamProperties,
  SetBrightness: SetBrightness,
  SetFloatValue: SetFloatValue,
  SetStringValue: SetStringValue,
  SetSleeping: SetSleeping,
  SetExposure: SetExposure,
  SetIntegerValue: SetIntegerValue,
  SetGamma: SetGamma,
  SetROI: SetROI,
  SetGain: SetGain,
  SetBinning: SetBinning,
};

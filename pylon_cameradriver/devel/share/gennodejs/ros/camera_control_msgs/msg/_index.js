
"use strict";

let currentParams = require('./currentParams.js');
let GrabHDRImageFeedback = require('./GrabHDRImageFeedback.js');
let GrabAndSaveImageAction = require('./GrabAndSaveImageAction.js');
let GrabImagesFeedback = require('./GrabImagesFeedback.js');
let GrabHDRImageActionGoal = require('./GrabHDRImageActionGoal.js');
let GrabAndSaveImageGoal = require('./GrabAndSaveImageGoal.js');
let GrabImagesAction = require('./GrabImagesAction.js');
let GrabAndSaveImageActionGoal = require('./GrabAndSaveImageActionGoal.js');
let GrabHDRImageActionResult = require('./GrabHDRImageActionResult.js');
let GrabImagesActionFeedback = require('./GrabImagesActionFeedback.js');
let GrabImagesGoal = require('./GrabImagesGoal.js');
let GrabImagesActionResult = require('./GrabImagesActionResult.js');
let GrabImagesResult = require('./GrabImagesResult.js');
let GrabHDRImageResult = require('./GrabHDRImageResult.js');
let GrabHDRImageAction = require('./GrabHDRImageAction.js');
let GrabImagesActionGoal = require('./GrabImagesActionGoal.js');
let GrabAndSaveImageActionResult = require('./GrabAndSaveImageActionResult.js');
let GrabHDRImageGoal = require('./GrabHDRImageGoal.js');
let GrabAndSaveImageActionFeedback = require('./GrabAndSaveImageActionFeedback.js');
let GrabHDRImageActionFeedback = require('./GrabHDRImageActionFeedback.js');
let GrabAndSaveImageResult = require('./GrabAndSaveImageResult.js');
let GrabAndSaveImageFeedback = require('./GrabAndSaveImageFeedback.js');

module.exports = {
  currentParams: currentParams,
  GrabHDRImageFeedback: GrabHDRImageFeedback,
  GrabAndSaveImageAction: GrabAndSaveImageAction,
  GrabImagesFeedback: GrabImagesFeedback,
  GrabHDRImageActionGoal: GrabHDRImageActionGoal,
  GrabAndSaveImageGoal: GrabAndSaveImageGoal,
  GrabImagesAction: GrabImagesAction,
  GrabAndSaveImageActionGoal: GrabAndSaveImageActionGoal,
  GrabHDRImageActionResult: GrabHDRImageActionResult,
  GrabImagesActionFeedback: GrabImagesActionFeedback,
  GrabImagesGoal: GrabImagesGoal,
  GrabImagesActionResult: GrabImagesActionResult,
  GrabImagesResult: GrabImagesResult,
  GrabHDRImageResult: GrabHDRImageResult,
  GrabHDRImageAction: GrabHDRImageAction,
  GrabImagesActionGoal: GrabImagesActionGoal,
  GrabAndSaveImageActionResult: GrabAndSaveImageActionResult,
  GrabHDRImageGoal: GrabHDRImageGoal,
  GrabAndSaveImageActionFeedback: GrabAndSaveImageActionFeedback,
  GrabHDRImageActionFeedback: GrabHDRImageActionFeedback,
  GrabAndSaveImageResult: GrabAndSaveImageResult,
  GrabAndSaveImageFeedback: GrabAndSaveImageFeedback,
};

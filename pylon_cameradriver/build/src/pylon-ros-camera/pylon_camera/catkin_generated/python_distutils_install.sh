#!/bin/sh

if [ -n "$DESTDIR" ] ; then
    case $DESTDIR in
        /*) # ok
            ;;
        *)
            /bin/echo "DESTDIR argument must be absolute... "
            /bin/echo "otherwise python's distutils will bork things."
            exit 1
    esac
fi

echo_and_run() { echo "+ $@" ; "$@" ; }

echo_and_run cd "/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/src/src/pylon-ros-camera/pylon_camera"

# ensure that Python install destination exists
echo_and_run mkdir -p "$DESTDIR/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/install/lib/python2.7/dist-packages"

# Note that PYTHONPATH is pulled from the environment to support installing
# into one location when some dependencies were installed in another
# location, #123.
echo_and_run /usr/bin/env \
    PYTHONPATH="/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/install/lib/python2.7/dist-packages:/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build/lib/python2.7/dist-packages:$PYTHONPATH" \
    CATKIN_BINARY_DIR="/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build" \
    "/usr/bin/python2" \
    "/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/src/src/pylon-ros-camera/pylon_camera/setup.py" \
     \
    build --build-base "/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build/src/pylon-ros-camera/pylon_camera" \
    install \
    --root="${DESTDIR-/}" \
    --install-layout=deb --prefix="/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/install" --install-scripts="/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/install/bin"

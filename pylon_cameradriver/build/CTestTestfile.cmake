# CMake generated Testfile for 
# Source directory: /home/meera/Basler drivers/basler-drivers/pylon_cameradriver/src
# Build directory: /home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("dragandbot_common/dnb_msgs")
subdirs("pylon-ros-camera/camera_control_msgs")
subdirs("pylon-ros-camera/pylon_camera")

# CMake generated Testfile for 
# Source directory: /home/meera/Basler drivers/basler-drivers/pylon_cameradriver/src/pylon-ros-camera/pylon_camera
# Build directory: /home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build/pylon-ros-camera/pylon_camera
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(_ctest_pylon_camera_roslaunch-check_launch "/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build/catkin_generated/env_cached.sh" "/usr/bin/python2" "/opt/ros/melodic/share/catkin/cmake/test/run_tests.py" "/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build/test_results/pylon_camera/roslaunch-check_launch.xml" "--return-code" "/usr/bin/cmake -E make_directory /home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build/test_results/pylon_camera" "/opt/ros/melodic/share/roslaunch/cmake/../scripts/roslaunch-check -o \"/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/build/test_results/pylon_camera/roslaunch-check_launch.xml\" \"/home/meera/Basler drivers/basler-drivers/pylon_cameradriver/src/pylon-ros-camera/pylon_camera/launch\" ")

#!/bin/bash

## Install pylon driver pylon_5.2.0.13457-deb0_amd64.deb as this is the stable scripting
## USB = 1
## Internet_pull = 2

#git clone https://gitlab.com/meera_kaze/basler-drivers.git
cd && cd basler-drivers && sudo dpkg -i pylon_5.2.0.13457-deb0_amd64.deb
echo "export PYLON_ROOT=/opt/pylon5" >> ~/.bashrc

git submodule init && git submodule update 

cd && cd basler-drivers/pylon_cameradriver && rm -r build/ && rm -r devel/ && catkin_make
catkin_make clean 
rosdep update
sudo sh -c 'echo "yaml https://raw.githubusercontent.com/basler/pylon-ros-camera/master/pylon_camera/rosdep/pylon_sdk.yaml " > /etc/ros/rosdep/sources.list.d/30-plyon_camera.list'
sudo rosdep install --from-paths . --ignore-src --rosdistro=$ROS_DISTRO -y
rosdep update
cd && cd basler-drivers/pylon_cameradriver && catkin_make
echo "source ~/basler-drivers/pylon_cameradriver/devel/setup.bash" >> ~/.bashrc
